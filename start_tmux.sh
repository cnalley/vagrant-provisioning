#!/bin/sh
# Setup a work space called `workspace` with two windows
# first window has 2 panes.

session="workspace"

# set up tmux
tmux start-server

# create a new tmux session
tmux -f ~/tmux.conf new-session -d -s $session 

# Select pane 1, set dir, get root
tmux selectp -t 1 
tmux send-keys "cd airliner" C-m


# Split pane 1 horizontal by 50%
tmux splitw -v -p 50
tmux send-keys "cd airliner" C-m


# Split pane 1 vertically by 50%
tmux splitw -h -p 50
tmux selectp -t 0 
tmux splitw -h -p 50
tmux send-keys "firefox -new-window https://tmuxcheatsheet.com/" C-m
tmux selectp -t 0

# create a new window called scratch
tmux new-window -t $session:1 -n scratch

# return to main vim window
tmux select-window -t $session:0

# Finished setup, attach to the tmux session!
tmux attach-session -t $session



