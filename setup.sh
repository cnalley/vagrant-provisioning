#!/bin/bash

echo "Provisioning as root"

# echo "Installing custom stuff"
apt-get --yes install tig
apt-get --yes install tmux
apt-get --yes install geany
mkdir /home/vagrant/.config/geany
cp /vagrant/geany.conf /home/vagrant/.config/geany
cp /vagrant/start_tmux.sh /home/vagrant/Desktop
cp /vagrant/stop_tmux.sh /home/vagrant/Desktop
cp /vagrant/tmux.conf /home/vagrant
chmod 777 /home/vagrant/.config/geany
chmod +x /home/vagrant/Desktop/start_tmux.sh
chmod +x /home/vagrant/Desktop/stop_tmux.sh

echo "Provisioning: copying key file..."
cp /vagrant/ssh.tar.gz /home/vagrant/Desktop

echo "Provisioning: decompressing keys..."
tar -zxvf /home/vagrant/Desktop/ssh.tar.gz -C /home/vagrant/Desktop

echo "Provisioning: copying decompressed keys..."
sudo -u vagrant -H sh -c "cp -f /home/vagrant/Desktop/.ssh/id_rsa.pub /home/vagrant/.ssh"
sudo -u vagrant -H sh -c "cp -f /home/vagrant/Desktop/.ssh/id_rsa /home/vagrant/.ssh"

echo "Setting up git config info"
sudo -u vagrant -H sh -c "git config --global user.email 'cnalley@windhoverlabs.com'"
sudo -u vagrant -H sh -c "git config --global user.name 'cnalley'"
sudo -u vagrant -H sh -c "git config --list"

echo "Provisioning done!"

